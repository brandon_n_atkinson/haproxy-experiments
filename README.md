# HAProxy Experiments 

Use docker as a lightweight vm for playing with haproxy:

    boot2docker
    docker run -it ubuntu:14.04.2 /bin/bash
    <do stuff interactively to figure out how to do it>
    <update the Dockerfile to do this automatically when building the image>

Install HAProxy (used haproxy:15.1.12 official image)

Create a self-signed SSL cert for *.xip.io

    sudo openssl genrsa -out xip.io.key 1024
    sudo openssl req -new -key xip.io.key -out xip.io.csr
    sudo openssl x509 -req -days 365 -in /etc/ssl/xip.io/xip.io.csr -signkey /etc/ssl/xip.io/xip.io.key -out /etc/ssl/xip.io/xip.io.crt
    sudo cat /etc/ssl/xip.io/xip.io.crt /etc/ssl/xip.io/xip.io.key | sudo tee /etc/ssl/xip.io/xip.io.pem

Create configuration file for haproxy w/ ssl

Create Dockerfile to document the build, and update it with the steps.

Create webserver (whoami) that returns its hostname and ip address for testing

    docker build -t brandonnatkinson/whoami whoami/

Create loadbalancer with ssl configuration

    docker build -t brandonnatkinson/haproxyssl haproxyssl/

Run webservers to give HAProxy something to which to offload

    docker run -d --name w1 brandonnatkinson/whoami
    docker run -d --name w2 brandonnatkinson/whoami

Run loadbalancer and link it up to the webserver containers

    docker run --name lb -d -p 80:80 -p 443:443 --link w1:w1 --link w2:w2 brandonnatkinson/haproxyssl

Hit the load balancer to to make sure that everything is working:

    http --verify=no get https://`boot2docker ip` #should see server 1
    http --verify=no get https://`boot2docker ip` #should see server 2
    http --verify=no get https://`boot2docker ip` #should see server 1
    http --verify=no get http://`boot2docker ip`  #should see redirect to https

Stop docker containers

    docker stop lb w1 w2

References

* https://bitbucket.org/brandon_n_atkinson/haproxy-experiments
* https://serversforhackers.com/using-ssl-certificates-with-haproxy
* https://serversforhackers.com/load-balancing-with-haproxy
* https://serversforhackers.com/self-signed-ssl-certificates
* http://docs.docker.com/reference/builder
