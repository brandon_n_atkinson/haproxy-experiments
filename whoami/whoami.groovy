import org.springframework.util.MultiValueMap

@RestController
class Whoami {
    @RequestMapping("/")
    def whoami(@RequestHeader MultiValueMap<String,String> headers) { 
        [hostname:InetAddress.localHost.hostName, ip:InetAddress.localHost.hostAddress, headers:headers]
    }
}
